<!doctype html>
<html lang="pt_BR">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Fabio Beneditto">
    <meta name="description" content="Como montar uma árvore a partir de um determinado número">
    <title>Tabuada</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/milligram/1.3.0/milligram.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/milligram/1.3.0/milligram.min.css.map" />
</head>
<body>
<?php
$numero = ( ! empty($_POST['numero'])) ? $_POST['numero'] : "";
?>

<main class="wrapper">
    <section class="container" id="form">
        <h1>Tabuada</h1>
        <div class="row">
            <div class="column">
                <form method="post">
                    <fieldset>
                        <label for="numero">Número para cálculo</label>
                        <input placeholder="Informe um número para calcular" id="numero" type="number" name="numero" value="<?php echo $numero; ?>">
                        <input class="button-primary" value="Enviar" type="submit">
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="column column-50 column-offset-25">
                <?php if ( ! empty($numero)){ ?>
                    <h3>Tabuada do número <?php echo $numero; ?></h3>
                <?php
                    for ($i = 0; $i <= 10; $i++){ // Linhas
                        echo $i . " X " . $numero . " = " . ($i * $numero);
                        echo "<br>";
                    }
                } ?>
            </div>
        </div>

    </section>

    <footer class="footer">
        <section class="container">
            <p>
                Por <a target="_blank" href="http://altoscodigos.com.br" title="Fabio Beneditto">Fabio Beneditto</a>.
            </p>
        </section>
    </footer>

</main>

</body>
</html>