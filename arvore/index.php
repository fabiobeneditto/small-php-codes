<!doctype html>
<html lang="pt_BR">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Fabio Beneditto">
    <meta name="description" content="Como montar uma árvore a partir de um determinado número">
    <title>Árvore</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/milligram/1.3.0/milligram.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/milligram/1.3.0/milligram.min.css.map" />
</head>
<body>
<?php
$numero = ( ! empty($_POST['numero'])) ? $_POST['numero'] : "";
?>

<main class="wrapper">
    <section class="container" id="form">
        <h1>Árvore</h1>
        <div class="row">
            <div class="column">
                <form method="post">
                    <fieldset>
                        <label for="numero">Número Inicial</label>
                        <input placeholder="Informe um número inicial" id="numero" type="number" name="numero" value="<?php echo $numero; ?>">
                        <input class="button-primary" value="Enviar" type="submit">
                    </fieldset>
                </form>
            </div>
            <div class="column column-10">&nbsp;</div>
            <div class="column column-center">
                <div align="center">
                    <?php if ( ! empty($numero)){
                        for ($i = 1; $i <= $numero; $i++){ // Linhas

                            for ($j = $i; $j >= 1; $j--){ // Esquerda
                                echo $j;
                            }

                            for ($k = 1; $k <= $i; $k++){ // Direita
                                if( $k > 1){ // Não repete o primeiro
                                    echo $k;
                                }
                            }

                            echo "<br>";
                        }
                    } ?>
                </div>

            </div>

        </div>

    </section>

    <footer class="footer">
        <section class="container">
            <p>
                Por <a target="_blank" href="http://altoscodigos.com.br" title="Fabio Beneditto">Fabio Beneditto</a>.
            </p>
        </section>
    </footer>

</main>
</body>
</html>